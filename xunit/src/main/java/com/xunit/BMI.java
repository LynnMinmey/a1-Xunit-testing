/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xunit;

/**
 *
 * @author Sabina Rosendahl
 */
public class BMI {
    
    private double length;
    private int weight;
    
    public BMI(){
        throw new IllegalStateException();
    }
    
    public BMI(double l, int w){
        this.length = l;
        this.weight = w;
        
    }
    
    public double calculateBMI(){
        return weight/(length*length);
    }
}
