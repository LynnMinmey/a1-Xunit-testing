/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.xunit.BMI;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 *
 * @author sabbe
 */
public class bmiTest {

    @Before
    public void setUp() {
        
    }
    
    @After
    public void tearDown() {
    }

     @Test(expected = IllegalStateException.class)
    public void testConstructorIllegalState() {
        BMI bmi  = new BMI();
    }
    
    @Test
    public void testConstructorValidState(){
        BMI bmi = new BMI(1.80, 70);
        assertNotNull(bmi);
    }
    
    @Test
    public void testCalculateBMI(){
        BMI bmi = new BMI(1.80, 70);
        assertNotNull(bmi.calculateBMI());
    }
    
    @Test
    public void testMocking(){
        BMI bmi = mock(BMI.class);
        when(bmi.calculateBMI()).thenReturn(22.0);
        assertEquals(22.0, bmi.calculateBMI(), 0.0);
    }
}
